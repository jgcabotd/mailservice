const mailgun = require('./services/mails/mailgunService.js');

const check = function (req, res, next) {
    if (req.body) {
        next();
    } else {
        res.status(500).send({
            date: new Date(),
            provider: req.route.path.split("/")[2],
            sent: false
        });
    }
}

class Router {
    constructor() {
        this.router = require("express").Router();
        this.init();
    }

    init() {
        this.router.post("/mailgun", check, mailgun.success);
    }

    getRouter() {
        return this.router;
    }
}

module.exports = new Router();