const settings = {
    apiKey: 'tu api key',
    domain: 'tu dominio de mailgun'
}

const mailgunService = require("mailgun-js")(settings);
const sender = 'Weekly Notifications <weeklynotifications@gmail.com>';
const contentFormat = require('../services/formatter.js');

module.exports = function (teacher) {
    return new Promise(function(res, rej) {
        const data = {
            from: sender,
            to: teacher.mail,
            subject: `Weekly notification for ${teacher.name} ${teacher.surname}`,
            html: contentFormat(teacher).toHtml()
        };

        mailgunService.messages().send(data, function (error) {
            if (error) {
                rej({
                    provider: 'Mailgun',
                    sent: false,
                    date: new Date()
                });
            }
            res({
                provider: 'Mailgun',
                sent: true,
                date: new Date()
            });
        });
    })
}