class Teacher {
    constructor({dni,id,mail,name,notifications ,phoneNum,surname}) {
        this.dni = dni;
        this.id = id;
        this.mail = mail;
        this.name = name;
        this.notifications = notifications;
        this.phoneNum = phoneNum;
        this.surname = surname;
    }

    setNotifications(notifications) {
        this.notifications = notifications;
    }
}

module.exports = Teacher;