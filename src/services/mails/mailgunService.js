const sendMail = require('../../configs/mailgun.config.js');
const createMail = require("../mailContainer.js");

module.exports = {
    success: async function (req, res) {
        const data = createMail(req.body);

        const {
            notifications
        } = data;

        const result = await sendMail(data).catch(err => err);
        result.idNotifications = notifications ? notifications.map(({
            id
        }) => parseInt(id)) : null;
        res.send(result);
    }
}

