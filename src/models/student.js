class Student {
    constructor({absences, dni, id, name, surname}) {
        this.absences = absences;
        this.dni = dni;
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    setAbsences(absences) {
        this.absences = absences;
    }
}

module.exports = Student;