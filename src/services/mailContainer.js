const Parser = require('./parser.js');

class MailContainer {
    constructor(emailData) {
        this.parser = new Parser(emailData);
        this.init();
    }

    init() {
        this.parser.parse();
    }

    getData() {
        return this.parser.getData();
    }

}

module.exports = function(data) {
    return new MailContainer(data).getData();
}