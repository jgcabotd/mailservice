class Notification {
    constructor({date, id, itWasSent, students, time}) {
        this.date = date;
        this.id = id;
        this.itWasSent = itWasSent;
        this.students = students;
        this.time = time;
    }

    setStudents(students) {
        this.students = students;
    }
}

module.exports = Notification;