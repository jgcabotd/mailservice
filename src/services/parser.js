const Teacher = require('../models/teacher.js');
const Notification = require('../models/notification.js');
const Student = require('../models/student.js');
const Absence = require('../models/absence.js');

module.exports = class Parser {
    constructor({dni, id, mail, name, notifications, phoneNum, surname}) {
        this.dni = dni;
        this.id = id;
        this.mail = mail;
        this.name = name;
        this.notifications = notifications;
        this.phoneNum = phoneNum;
        this.surname = surname;
    }

    parse() {
        if (!this.notifications) {
            return [];
        }

        return this.notifications.map(element => {
            const notification = new Notification(element);
            notification.setStudents(this.parseStudents(notification))
            return notification;
        });
    }

    parseStudents({students}) {
        if (!students) { 
            return [];
        }

        return students.map(element => {
            const student = new Student(element);
            student.setAbsences(this.parseAbsences(student));
            return student;
        })
    }

    parseAbsences({absences}) {
        if (!absences) {
            return [];
        }

        return absences.map(absence => {
            return new Absence(absence);
        })
    }

    getData() {
        return new Teacher(this);
    }

}