class Formatter {
    constructor(data) {
        this.data = data;
    }

    toHtml() {
        const html = `
        <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Notification</title>
    <style>

        * {
            box-sizing: border-box;
        }

        body {
            background-color: #ddd;
            font-family: 'Source Sans Pro';
            font-weight: 300;
        }

        .Message {
            display: table;
            position: relative;
            margin: 20px auto 0;
            width: 500px;
            background-color: whitesmoke;
            color: black;
            transition: all 0.2s ease;
            border-radius: 10px;
        }

        .Message.is-hidden {
            opacity: 0;
            height: 0;
            font-size: 0;
            padding: 0;
            margin: 0 auto;
            display: block;
        }


        .Message--red {
            background-color: #b33939;
            font-family: Arial, Helvetica, sans-serif;
            color: white;
        }

        .Message-icon {
            display: table-cell;
            vertical-align: middle;
            width: 20px;
            padding: 10px;
            text-align: center;
            background-color: rgba(0, 0, 0, 0.25);
            border-radius: 10px 0px 0px 10px;
        }

        .Message-body {
            display: table-cell;
            vertical-align: middle;
            padding: 5px 25px 5px 25px;
        }

        li {
            list-style: none;
        }

    </style>
</head>

<body>
    <div class="Message" id="js-timer">
        <div class="Message-icon">
        </div>
        <div class="Message-body">
            <h1 style="color: black;">Hola ${this.data.name},</h1>
            <p style="color: black; font-size: 20px;">Aquí tens els retards que falten validar</p>
            
            ${this.getNotifications().join("")}
        </div>
    </div>

</body>

</html>
        `
        return html;
    }

    getNotifications() {
        return this.data.notifications.map(not => {
            const html = `<div style="padding-bottom: 15px;" class="Message Message--red">
    <img src="http://172.16.7.228:3000/${not.id}/empty.png" alt="">

            <div class="Message-icon">
            </div>
            <div class="Message-body">
                ${this.getStudents(not).join("")}
            </div>
        </div>`
            return html;
        })
    }

    getStudents(notification) {
        return notification.students.map(stud => {
            const html = `<h4 style="color: white; font-size: 14pt !important; margin: 0px; padding: 10px 10px 5px 10px;">Faltes d'en ${stud.name} ${stud.surname}: </h4>
            <ul style="margin: 0px; padding: 5px 10px 10px 10px;">
                ${this.getAbsencies(stud).join("")}
            </ul>`
            return html;
        })
    }

    getAbsencies(student) {
        return student.absences.map(({
            date,
            subject,
            time
        }) => {
            return `<li style="font-size: 12pt;">${subject} - ${date}-${time}</li>`
        })
    }

}

module.exports = function (obj) {
    return new Formatter(obj);
}