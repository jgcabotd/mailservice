// Global server
const express = require("express");
const app = express();
const server = require("http").createServer(app);
const bodyParser = require("body-parser");
const morgan = require('morgan');
const fetch = require("node-fetch");
// Router
const router = require('./src/router.js').getRouter();

// Server params
const ip = "localhost";
const port = 3001;

// Set up basic middlewares
app.set('json spaces', 40);

//app.use(morgan("dev"))
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

// Routes for each service
app.use("/notify", router);

app.get("/:id/empty.png", async function(req,res) {
    const id = req.params.id;
    await fetch("http://172.16.7.230:8081/setRead", {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(parseInt(id))
    })

    res.sendFile(__dirname +'/empty.png')
})


// Error 404 handler
app.use(function (req, res) {
    res.status(404).send("I think you got the wrong path mate")
});

// Start server
server.listen(port, ip, function () {
    console.log(`http://${ip}:${port}`);
})